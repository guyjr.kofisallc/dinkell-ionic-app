// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC5duyFu1PtTygR219ydqbxFZ36Pn5auUA",
    authDomain: "dinkell-ce07b.firebaseapp.com",
    projectId: "dinkell-ce07b",
    storageBucket: "dinkell-ce07b.appspot.com",
    messagingSenderId: "628433833755",
    appId: "1:628433833755:web:04484e37ba9e92d8fb4dbe",
    measurementId: "G-KRV86YFMEZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
